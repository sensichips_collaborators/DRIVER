#include "CommTCP.h"


CommTCP::CommTCP(){
  //this->status = s;
}

void CommTCP::init(WiFiClient *client, WiFiServer *server){
  this->client = client;
  this->server = server;
}

boolean CommTCP::rcvByte(byte &rcvdByte){
  // Wait for rcvdByte
  while(client->available() <= 0 && client->connected());
  if (client->connected()){
    rcvdByte = client->read();
    return true;
  } else {
    return false;
  }
}


void CommTCP::receiveHeader(MsgSENSIBUS &SBMsg, StatusSENSIBUS &status){
  
  if (rcvByte(SBMsg.command)){
    //SBMsg.command = Serial.read();
    writeOnDebug(0xA0);
    writeOnDebug(SBMsg.command);
    
    if (SBMsg.command & EXT_INSTR){
      //status.instructionType = EXT_INSTR; 
      // TODO: manage ext instruction
      SBMsg.instructionType = SBMsg.command;
      //writeOnDebug(0xA0);
   
    } else {
      // Manage normal instruction
      //*** Receive address
      status.addressingMode = SBMsg.command & ADDRESS_MODE_MASK;
      
      if (status.addressingMode == LastAddressValue){
        status.addressingMode = status.lastAddressingMode;
      } else {
        status.lastAddressingMode = status.addressingMode;
        if (status.addressingMode == NoAddressValue) {
           // No addressing byte to receive
           SBMsg.chipAddressLength = 0;
    
        } else if (status.addressingMode == ShortAddressValue) {
          SBMsg.chipAddressLength = 1;
        } else if (status.addressingMode == FullAddressValue) {
          SBMsg.chipAddressLength = 6;
        }
      }
      SBMsg.dataRcvLength = SBMsg.command & DATA_LENGTH_MASK;
      writeOnDebug(0xA1);
      writeOnDebug(SBMsg.command);
      writeOnDebug(SBMsg.dataRcvLength);
      SBMsg.instructionType = NORMAL_INSTR;
    }
  } else {
    SBMsg.instructionType = NO_INSTR;
  }

  return;
}


int CommTCP::receiveData(MsgSENSIBUS &SBMsg, StatusSENSIBUS &status){
    int len = 0;
    //SBMsg.dataRcvLength = 0;
    
    
    if (SBMsg.instructionType & EXT_INSTR) {

       if ((SBMsg.instructionType & EXT_INSTR_USBSPEED_MASK) == EXT_INSTR_USBSPEED_MASK){

          SBMsg.dataRcvLength = 1;
          rcvByte(SBMsg.dataRcv[0]);
          //Serial.readBytes(SBMsg.dataRcv, SBMsg.dataRcvLength);
          //newUSBSpeed = USB_SPEED[SBMsg.dataRcv[0]];
          // copyArray(buffer, SBMsg.dataRcv, SBMsg.dataRcvLength);
          //Serial.write(0xA3);
          //Serial.write(SBMsg.dataRcv[0]);

       
       } else if ((SBMsg.instructionType & EXT_INSTR_CHIPLIST_MASK) == EXT_INSTR_CHIPLIST_MASK){
          byte NumOfChips[1];
          //Serial.write(0xE1);
          rcvByte(NumOfChips[0]); // Read the number of chips
          //Serial.readBytes(NumOfChips, 1); // Read the number of chips
          SBMsg.dataRcvLength = 1 + 1 + 1 + 6 * NumOfChips[0];
          
          for(int i = 0; i < SBMsg.dataRcvLength; i++){
             rcvByte(SBMsg.dataRcv[i]);
          }
          // Serial.readBytes(SBMsg.dataRcv, SBMsg.dataRcvLength);
          

          // copyArray(buffer, SBMsg.dataRcv, SBMsg.dataRcvLength);
          // 1st byte = Number of chips on the cable
          // 2nd byte = cluster id
          // 3nd byte = multicast id
          // other 6*NumOfChips bytes are the id of the chip on the cable.
       }
       
    } else { // NORMAL_INSTR
    
      //**** Receive address
      int len = 0;
      //**** Receive address
      for(int i = 0; i < SBMsg.chipAddressLength; i++){
        rcvByte(SBMsg.chipAddress[i]);
      }
      len = SBMsg.chipAddressLength;
      //len = Serial.readBytes(SBMsg.chipAddress, SBMsg.chipAddressLength);
      
      //**** Receive register
      //Serial.readBytes(SBMsg.registerAddress, 1);
      rcvByte(SBMsg.registerAddress[0]);

      writeOnDebug(0xB0);
      writeOnDebug(SBMsg.registerAddress[0]);
      writeOnDebug(SBMsg.dataRcvLength);
  
      // Establish if is a read or write operation (1 read, 0 false)
      status.isRead = IS_WRITE_MASK & SBMsg.registerAddress[0];
  
      //**** Receive data
      if (status.isRead){
        for(int i = 0; i < SBMsg.dataRcvLength; i++){
          SBMsg.dataRcv[i] = 0xFF;                        // FF in order to wait for data 
        }
        len = SBMsg.dataRcvLength;
      } else {
        for(int i = 0; i < SBMsg.dataRcvLength; i++){
           rcvByte(SBMsg.dataRcv[i]);
        }
  len = SBMsg.dataRcvLength;
        //len = Serial.readBytes(SBMsg.dataRcv, SBMsg.dataRcvLength);
      }
    }
    return len;
}


void CommTCP::sendResponse(MsgSENSIBUS &SBMsg){
  writeOnDebug(0xD0);
  for(int i = 0; i < SBMsg.dataToHostLength; i++){
    writeOnDebug(SBMsg.dataToHost[i]);
    client->write(SBMsg.dataToHost[i]);
  }  
}

